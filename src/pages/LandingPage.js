import React from "react";
import Footer from '../components/Footer.js';
import landingpage from '../pictures/landingpage.jpg'
import { Link } from "react-router-dom";
import JobList from "../components/JobList.js";

const LandingPage = () =>{
    return(
        <>
            <div className="grid">
                <div className="bg-gray-200 align-starts relative overflow-hidden h-screen">
                    <img src={landingpage} className=" absolute h-full w-full object-cover" />
                    <div className="inset-0 bg-black opacity-25 absolute">
                    </div>
                    <div className="container mx-auto px-6 md:px-12 relative z-10 flex items-center py-32 xl:py-40">
                        <div className="lg:w-3/5 xl:w-2/5 flex flex-col items-start relative z-10">
                            <h1 className="font-bold italic text-5xl text-white leading-tight mt-4">
                                "If opportunity doesn't knock
                                <br />
                                build a door"
                            </h1>
                            <span className="font-bold uppercase text-yellow-400 mt-2">
                                Milton Berle
                            </span>
                            <Link to={'vacancy/list'} className="block bg-white hover:bg-gray-100 py-3 px-4 rounded-lg text-lg text-gray-800 font-bold uppercase mt-10">
                                Job List
                            </Link>
                        </div>
                    </div>
                </div>
                <div className="align-end">
                    <Footer />
                </div>
            </div>
        </>
    )
}

export default LandingPage