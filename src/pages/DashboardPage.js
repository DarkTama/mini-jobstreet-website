import React,{useContext} from "react";
import Footer from "../components/Footer";
import Sidebar from "../components/Sidebar";
import {Outlet} from 'react-router-dom';
import { GlobalContext } from "../context/GlobalContext";

const DashboardPage = () =>{
    const{state, handleFunction} = useContext(GlobalContext)
    const{data} = state
    return(
        <>
            <main className="bg-gray-100 dark:bg-gray-800 h-screen overflow-hidden relative">
                <div className="flex items-start justify-between">
                    <Sidebar />
                    <div className="flex flex-col w-full md:space-y-4">
                        <div className="overflow-auto h-screen max-w-3/4 pb-24 px-4 md:px-6">
                            <br/>
                            <Outlet />
                        </div>
                    </div>
                </div>
            </main>
            {/* <Footer /> */}
        </>
    )
}
export default DashboardPage