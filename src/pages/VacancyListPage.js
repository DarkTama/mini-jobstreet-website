import React from "react";
import Footer from '../components/Footer.js';
import landingpage from '../pictures/landingpage.jpg'
import { Link, Outlet } from "react-router-dom";
import JobList from "../components/JobList.js";

const VacancyListPage = () =>{
    return(
        <>
            <div className="bg-gray-200 relative h-screen">
                <img src={landingpage} className="blur-sm absolute h-full w-full object-cover" />
                <div className="inset-0 bg-black opacity-25 absolute">
                </div>
                <div className="container mx-auto px-6 md:px-12 relative z-10 flex-wrap items-center py-24 xl:py-32">
                    {/* <JobList /> */}
                    <Outlet />
                </div>
                <Footer />
            </div>
        </>
    )
}

export default VacancyListPage