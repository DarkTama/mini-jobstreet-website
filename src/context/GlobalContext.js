import { createContext, useState, useEffect, useRef } from "react";
import axios from 'axios'
import { useNavigate } from "react-router-dom";
import Cookies from "js-cookie";

export const GlobalContext = createContext();

export const GlobalProvider = (props) =>{
    const [data, setData] = useState(null)
    const [vacancy, setVacancy] = useState(null)
    const [fetchStatus, setFetchStatus] = useState(true)
    const [success, setSuccess] = useState()
    const [validasi, setValidasi] = useState()
    const [filterCompany, setFilterCompany] = useState(null)
    const [filterType, setFilterType] = useState(null)
    const [filterTenure, setFilterTenure] = useState(null)
    const navigate = useNavigate()
    const [currentId, setCurrentId] = useState(-1)
    const [filters, setFilters] = useState({
        job_location:"",
        job_type:"",
        job_tenure:""
    })
    const [input, setInput] = useState({
        company_city:"",
        company_name:"",
        company_image_url:"",
        job_description:"",
        job_qualification:"",
        job_status:"",
        job_tenure:"",
        job_type:"",
        salary_max:"",
        salary_min:"",
        title:"",
        name:"",
        image_url:"",
        email:"",
        password:"",
        confirmpassword:"",
        current_password:"",
        new_password:"",
        new_confirm_password:""
    })
    
    const fetch = () =>{
        setFilters({
            job_location:"",
            job_type:"",
            job_tenure:""
        })
        setFetchStatus(true)
    }

    const removeDuplicateCompanyCity = (e) =>{
        let tmp = []
        // console.log(e)
        for(let i of e){
            if(tmp.indexOf(i.company_city) === -1){
                tmp.push(i.company_city)
            }else if (tmp.indexOf(i.company_city) === null){

            }
        }
        return(tmp)
    }

    const removeDuplicateJobType = (e)=>{
        let tmp = []
        // console.log(e)
        for(let i of e){
            if(tmp.indexOf(i.job_type) === -1){
                tmp.push(i.job_type)
            }else if (tmp.indexOf(i.job_type) === null){
                
            }
        }
        return(tmp)
    }

    const removeDuplicateJobTenure = (e)=>{
        let tmp = []
        // console.log(e)
        for(let i of e){
            if(tmp.indexOf(i.job_tenure) === -1){
                tmp.push(i.job_tenure)
            }else if (tmp.indexOf(i.job_tenure) === null){
                
            }
        }
        return(tmp)
    }

    const filterBySearch = (e) => {
        let query = e.target.value
        if(query !== null){
            axios.get('https://dev-example.sanbercloud.com/api/job-vacancy')
            .then((res)=>{
                let data = res.data.data
                // console.log(data)
                let searchData = data.filter((ress)=>{
                    return ress.title.toLowerCase().includes(query.toLowerCase())
                })
                // console.log(searchData)
                setData([...searchData])
            })
            .catch((err)=>{
                console.log(err)
            })
        }else{
            setFetchStatus(true)
        }
    };

    useEffect( () => {
        if(fetchStatus===true){
            setTimeout(() => {
                setSuccess("")
                setValidasi("")
            }, 5000);
            axios.get('https://dev-example.sanbercloud.com/api/job-vacancy')
            .then((res)=>{
                let data = res.data.data
                let filter = data.map((res)=>{
                    let{
                        company_city,
                        job_type,
                        job_tenure
                    } = res
                    return{
                        company_city,
                        job_type,
                        job_tenure
                    }
                })
                // console.log(data)
                let filteredCompanyCity = removeDuplicateCompanyCity(filter)
                let filteredJobTenure = removeDuplicateJobTenure(filter)
                let filteredJobType = removeDuplicateJobType(filter)
                setFilterCompany([...filteredCompanyCity])
                setFilterTenure([...filteredJobTenure])
                setFilterType([...filteredJobType])
                setData([...data])
                setInput({
                    company_city:"",
                    company_name:"",
                    company_image_url:"",
                    job_description:"",
                    job_qualification:"",
                    job_status:"",
                    job_tenure:"",
                    job_type:"",
                    salary_max:"",
                    salary_min:"",
                    title:"",
                    name:"",
                    image_url:"",
                    email:"",
                    password:"",
                    confirmpassword:"",
                    current_password:"",
                    new_password:"",
                    new_confirm_password:""
                })
                setFetchStatus(false)
            })
            .catch((err)=>{
                console.log(err)
            })
        }
    },[fetchStatus,setFetchStatus])

    const handleTruncate = (str, panjang) =>{
        if(str===null) return ""
        return str.length > panjang ? str.substring(0, panjang-3) + "..." : str
    }

    const handleInput = (e) =>{
        setSuccess("")
        setValidasi("")
        let name = e.target.name
        let value = e.target.value
        setInput({...input, [name]:value})
        console.log(input)
    }

    const handleSubmit = (e) =>{
        e.preventDefault()
        let{
            company_city,
            company_name,
            company_image_url,
            job_description,
            job_qualification,
            job_status,
            job_tenure,
            job_type,
            salary_max,
            salary_min,
            title,
        } = input
        if (currentId===-1){
            axios.post('https://dev-example.sanbercloud.com/api/job-vacancy',{
                company_city,
                company_name,
                company_image_url,
                job_description,
                job_qualification,
                job_status,
                job_tenure,
                job_type,
                salary_max,
                salary_min,
                title,
            },
            {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}})
            .then((res)=>{
                console.log(res)
                setSuccess("Menambah Data Berhasil!")
                setFetchStatus(true)
            })
            .catch((err)=>{
                console.log(err)
                setValidasi("Menambah Data Gagal!")
            })
        }
        else{
            axios.put(`https://dev-example.sanbercloud.com/api/job-vacancy/${currentId}`,{
                company_city,
                company_name,
                company_image_url,
                job_description,
                job_qualification,
                job_status,
                job_tenure,
                job_type,
                salary_max,
                salary_min,
                title,
            },
            {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}})
            .then((res)=>{
                console.log(res)
                setSuccess("Mengubah Data Berhasil!")
                setFetchStatus(true)
            })
            .catch((err)=>{
                console.log(err)
                setValidasi("Mengubah Data Gagal!")
            })
        }
    }

    const handleEdit = (e) =>{
        let idEdit = parseInt(e.target.value)
        setCurrentId(idEdit)
        navigate(`/dashboard/list-job-vacancy/form/${idEdit}`)
    }

    const handleDelete = (e) =>{
        let idDelete = parseInt(e.target.value)
        axios.delete(`https://dev-example.sanbercloud.com/api/job-vacancy/${idDelete}`,
        {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}})
        .then((res)=>{
            setFetchStatus(true)
            setSuccess("Menghapus data berhasil!")
        })
        .catch((err)=>{
            setValidasi("Menghapus data gagal! Cek console!")
            console.log(err)
        })
    }

    const handleRegister = (e) =>{
        e.preventDefault()
        let{
            name,
            image_url,
            email,
            password,
            confirmpassword
        } = input
        if(confirmpassword===password){
            axios.post("https://dev-example.sanbercloud.com/api/register",{
                name,
                image_url,
                email,
                password
            }).then((res)=>{
                navigate("/login")
                console.log(res)
                setValidasi("")
                setSuccess("Register berhasil, silahkan Login!")
                setFetchStatus(true)
            }).catch((err)=>{
                console.log(err)
            })
        }
        else if(confirmpassword!==password){
            setValidasi("Password dan Confirm Password harus sama!")
        }
    }

    const handleLogin = (e) =>{
        e.preventDefault()
        let{
            email,
            password,
        } = input
        axios.post("https://dev-example.sanbercloud.com/api/login",{
            email, password
        }).then((res)=>{
            let {token} = res.data
            Cookies.set('token', token)
            navigate('/')
            console.log(res)
            setValidasi("")
            setFetchStatus(true)
        }).catch((err)=>{
            console.log(err)
            setValidasi("Identitas yang dimasukkan salah atau tidak terdaftar dalam server!")
        })
    }

    const handleChangePassword = (e) =>{
        e.preventDefault()
        let{
            current_password,
            new_password,
            new_confirm_password
        } = input
        axios.post('https://dev-example.sanbercloud.com/api/change-password',{
            current_password,
            new_password,
            new_confirm_password
        },
        {headers: {"Authorization" : "Bearer "+ Cookies.get('token')}})
        .then((res)=>{
            console.log(res)
            setSuccess("Mengubah Password Berhasil!")
            setFetchStatus(true)
        })
        .catch((err)=>{
            console.log(err)
            setValidasi("Mengubah Password Gagal!")
        })
    }

    const handleJobStatus = (e) =>{
        if (e!==0){
            return(
                <>
                <div className="text-xs mr-2 py-1.5 px-4 text-gray-600 bg-green-100 rounded-2xl">
                    Available
                </div>
                </>
            )
        }
        else{
            return(
                <>
                <div className="text-xs mr-2 py-1.5 px-4 text-gray-600 bg-red-100 rounded-2xl">
                    Unavailable
                </div>
                </>
            )
        }
    }

    const handleVacancyDetails = (e) =>{
        let idVacancy = parseInt(e.target.value)
        navigate(`vacancy/${idVacancy}`)
    }

    const handleChangeFilter = (e) =>{
        setFilters({...filters, [e.target.name]: e.target.value})
        console.log(filters)
    }
    
    const handleFilter = (e) =>{
        e.preventDefault()
        axios.get('https://dev-example.sanbercloud.com/api/job-vacancy')
            .then((res)=>{
                let dataJob = res.data.data
                // console.log(dataJob)
                let filterData = dataJob.filter((ress)=>{
                    if (filters.job_location !== ""){
                        return ress.company_city.toLowerCase() === filters.job_location.toLowerCase()
                    }if(filters.job_type !== ""){
                        return ress.job_type.toLowerCase() === filters.job_type.toLowerCase()
                    }if(filters.job_tenure !== ""){
                        return ress.job_tenure.toLowerCase() === filters.job_tenure.toLowerCase()
                    }
                })
                // console.log(filters)
                // console.log(filterData)
                setData([...filterData])
            })
            .catch((err)=>{
                console.log(err)
            })
    }

    const handlePrice = (angka) =>{
        if(angka===null){angka=0}
        var rupiah = '';		
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+'.';
        return 'Rp. '+rupiah.split('',rupiah.length-1).reverse().join('');
    }

    let state ={
        data, setData,
        input, setInput,
        validasi, setValidasi,
        fetchStatus, setFetchStatus,
        success, setSuccess,
        vacancy, setVacancy,
        filters, setFilters,
        filterCompany, filterTenure, filterType,
        navigate, fetch
    }
    let handleFunction ={
        handleTruncate,
        filterBySearch,
        handleInput,
        handleRegister,
        handleLogin,
        handleJobStatus,
        handleChangePassword,
        handlePrice,
        handleVacancyDetails,
        handleChangeFilter,
        handleSubmit,
        handleEdit,
        handleDelete,
        handleFilter
    }
    return(
        <GlobalContext.Provider value={
            {
                state, handleFunction
            }
        }>
            {props.children}
        </GlobalContext.Provider>
    )
}

