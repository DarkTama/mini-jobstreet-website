import React, { useContext } from "react";
import { GlobalContext } from "../context/GlobalContext";

const DataTable = () =>{
    const{state, handleFunction} = useContext(GlobalContext)
    const{data,success,validasi, filters, filterCompany, filterType, filterTenure, fetch} = state
    const{handleTruncate, filterBySearch, handleDelete, handleEdit, handleJobStatus, handleFilter, handleChangeFilter} = handleFunction
    return(
        <>
        {/* <div className="container mx-auto px-4 sm:px-8"> */}
            <div className="px-5 bg-white py-5 shadow rounded-lg overflow-hidden xs:flex-row items-center xs:justify-between">
                <input required onChange={filterBySearch} placeholder="Type job name..." name="searchbytitle" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500"/><br/>
                <form onSubmit={handleFilter} className="grid grid-cols-3">
                    <select onChange={handleChangeFilter} value={filters.job_location} name="job_location" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500">
                        <option Value="">Select Job Location. . .</option>
                        {filterCompany !== null &&(
                            <>
                            {filterCompany.map((res)=>{
                                return(
                                    <>
                                        <option defaultValue={`${res}`}>{res}</option>
                                    </>
                                )
                            })}
                            </>
                        )}
                    </select>
                    <select onChange={handleChangeFilter} value={filters.job_tenure} name="job_tenure" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500">
                        <option Value="">Select Job Tenure. . .</option>
                        {filterTenure !== null &&(
                            <>
                            {filterTenure.map((res)=>{
                                return(
                                    <>
                                        <option defaultValue={`${res}`}>{res}</option>
                                    </>
                                )
                            })}
                            </>
                        )}
                    </select>
                    <select onChange={handleChangeFilter} value={filters.job_type} name="job_type" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500">
                        <option Value="">Select Job Type. . .</option>
                        {filterType !== null &&(
                            <>
                            {filterType.map((res)=>{
                                return(
                                    <>
                                        <option defaultValue={`${res}`}>{res}</option>
                                    </>
                                )
                            })}
                            </>
                        )}
                    </select>
                    <button className="w-14 h-7 col-span-3 border border-gray-500 hover:bg-blue-500 bg-blue-400 rounded-lg" type="submit">Filter</button>
                </form>
                <button className="w-14 h-7 col-span-3 border border-gray-500 hover:bg-red-500 bg-red-400 rounded-lg" onClick={fetch}>Reset</button>
            </div>
            <div className="mt-5">
                <div className="-mx-4 sm:-mx-8 px-4 sm:px-8 py-4 overflow-x-auto">
                    <div className="inline-block shadow rounded-lg overflow-hidden">
                        <p className="font-bold text-green-500">{success}</p>
                        <p className="font-bold text-red-500">{validasi}</p>
                        <table>
                        <thead>
                            <tr>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                No
                            </th>    
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                ACT
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Title
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Deskripsi
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Kualifikasi
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Tipe
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Tenure
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Perusahaan
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Lokasi
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Min Salary
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Max Salary
                            </th>
                            <th scope="col" className="px-5 py-3 bg-white  border-b border-gray-200 text-gray-800  text-left text-sm uppercase font-normal">
                                Status
                            </th>
                            </tr>
                        </thead>
                        <tbody>
                            {data !== null && data.map((res, index)=>{
                                return(
                                    <tr>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {index +1}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <button value={res.id} onClick={handleEdit} className="w-14 h-auto border border-gray-300 rounded bg-yellow-300 hover:bg-yellow-400">Edit</button>&nbsp;
                                            <button value={res.id} onClick={handleDelete} className="w-14 h-auto border border-gray-300 rounded bg-red-500 hover:bg-red-600">Delete</button>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {res.title}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {handleTruncate(res.job_description, 30)}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {handleTruncate(res.job_qualification,30)}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {res.job_type}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {res.job_tenure}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {res.company_name}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {res.company_city}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {res.salary_min}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            <p className="text-gray-900 whitespace-no-wrap">
                                            {res.salary_max}
                                            </p>
                                        </td>
                                        <td className="px-5 py-5 border-b border-gray-200 bg-white text-sm">
                                            {handleJobStatus(res.job_status)}
                                        </td>
                                    </tr>
                                )
                            })}
                        </tbody>
                        </table>
                    </div>
                </div>
            </div>
        {/* </div> */}
        </>
    )
}
export default DataTable