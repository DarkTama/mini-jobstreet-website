import React from "react";
import {Link} from 'react-router-dom';
import Cookies from "js-cookie";
import { useNavigate } from "react-router-dom";

const Sidebar = () =>{
    const navigate = useNavigate()
    return(
        <>
            <div className="relative bg-white dark:bg-gray-800">
                <div className="flex flex-col sm:flex-row sm:justify-around">
                    <div className="w-64 h-screen">
                    <nav className="mt-10 px-6 ">
                        <ul>
                            <Link to="list-job-vacancy">
                                <li className="hover:text-gray-800 hover:bg-gray-100 flex items-center p-2 my-6 transition-colors dark:hover:text-white dark:hover:bg-gray-600 duration-200  text-gray-600 dark:text-gray-400 rounded-lg ">
                                    Data Table        
                                </li>
                            </Link>
                            <Link to="list-job-vacancy/form">
                                <li className="hover:text-gray-800 hover:bg-gray-100 flex items-center p-2 my-6 transition-colors dark:hover:text-white dark:hover:bg-gray-600 duration-200  text-gray-600 dark:text-gray-400 rounded-lg ">
                                    Data Form
                                </li>
                            </Link>
                            {/* <Link to="profile">
                                <li className="hover:text-gray-800 hover:bg-gray-100 flex items-center p-2 my-6 transition-colors dark:hover:text-white dark:hover:bg-gray-600 duration-200  text-gray-600 dark:text-gray-400 rounded-lg ">
                                    Profile
                                </li>
                            </Link> */}
                            <Link to="change-password">
                                <li className="hover:text-gray-800 hover:bg-gray-100 flex items-center p-2 my-6 transition-colors dark:hover:text-white dark:hover:bg-gray-600 duration-200  text-gray-600 dark:text-gray-400 rounded-lg ">
                                    Change Password
                                </li>
                            </Link>
                            {/* <Link> */}
                                <li onClick={() => {
                                Cookies.remove('token')
                                navigate('/')}} className="cursor-pointer hover:text-gray-800 hover:bg-gray-100 flex items-center p-2 my-6 transition-colors dark:hover:text-white dark:hover:bg-gray-600 duration-200  text-gray-600 dark:text-gray-400 rounded-lg ">
                                    Logout
                                </li>
                            {/* </Link> */}
                        </ul>
                    </nav>
                    </div>
                </div>
            </div>
        </>
    )
}
export default Sidebar