import React, {useContext} from "react";
import { Link } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

const Register = () =>{
    const{state, handleFunction} = useContext(GlobalContext)
    const{validasi, success, input} = state
    const{handleInput,handleRegister} = handleFunction
    return(
        <>
        <div className="grid place-items-center h-screen">
        <div className="flex flex-col max-w-md drop-shadow-xl px-4 py-8 bg-white rounded-lg shadow dark:bg-gray-800 sm:px-6 md:px-8 lg:px-10">
                <div className="self-center mb-2 text-xl font-light text-gray-800 sm:text-2xl dark:text-white">
                    Create a new account
                </div>
                <span className="justify-center text-sm text-center text-gray-500 flex-items-center dark:text-gray-400">
                    Already have an account ?
                    <a className="text-sm text-blue-500 underline hover:text-blue-700">
                        <Link to='/login'>
                        Sign in
                        </Link>
                    </a>
                </span>
                <div className="p-6 mt-8">
                    <form onSubmit={handleRegister}>
                    <div className="flex flex-col mb-2">
                        <div className=" relative ">
                        <input value={input.name} onChange={handleInput} required type="text" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="name" placeholder="Full Name" />
                        </div>
                    </div>
                    <div className="flex flex-col mb-2">
                        <div className=" relative ">
                        <input value={input.email} onChange={handleInput} required type="text" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="email" placeholder="E-Mail" />
                        </div>
                    </div>
                    <div className="flex flex-col mb-2">
                        <div className=" relative ">
                        <input value={input.password} onChange={handleInput} required minLength={8} type="password" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="password" placeholder="Password" />
                        </div>
                    </div>
                    <div className="flex flex-col mb-2">
                        <p className="text-red-500 font-bold">{validasi}</p>
                        <div className=" relative ">
                        <input value={input.confirmpassword} onChange={handleInput} required minLength={8} type="password" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="confirmpassword" placeholder="Confirm Password" />
                        </div>
                    </div>
                    <div className="flex flex-col mb-2">
                        <div className=" relative ">
                        <input value={input.image_url} onChange={handleInput} required type="text" className=" rounded-lg border-transparent flex-1 appearance-none border border-gray-300 w-full py-2 px-4 bg-white text-gray-700 placeholder-gray-400 shadow-sm text-base focus:outline-none focus:ring-2 focus:ring-purple-600 focus:border-transparent" name="image_url" placeholder="Profile Image URL" />
                        </div>
                    </div>
                    <div className="flex w-full my-4">
                        <p className="text-green-500 font-bold">{success}</p>
                        <button type="submit" className="py-2 px-4  bg-purple-600 hover:bg-purple-700 focus:ring-purple-500 focus:ring-offset-purple-200 text-white w-full transition ease-in duration-200 text-center text-base font-semibold shadow-md focus:outline-none focus:ring-2 focus:ring-offset-2  rounded-lg ">
                        Register
                        </button>
                    </div>
                    </form>
                </div>
            </div>
        </div>
        </>
    )
}

export default Register