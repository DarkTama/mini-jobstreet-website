import axios from "axios";
import React,{useContext, useEffect, useState} from "react";
import { Link, useParams } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

const VacancyDetails = () =>{
    const [data, setData] = useState(null)
    const [fetch,setFetch] = useState(true)
    let {idVacancy} = useParams()
    useEffect(()=>{
        if(idVacancy !== undefined){
            if(fetch===true){    
                axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy/${idVacancy}`)
                .then((res)=>{
                    console.log(res)
                    setData(res.data)
                })
                .catch((err)=>{
                    console.log(err)
                })
                setFetch(false)
            }
        }
    },[fetch,setFetch])
    const{state, handleFunction} = useContext(GlobalContext)
    const{handleJobStatus, handlePrice} = handleFunction
    return(
        <>
            <section className="bg-gray-200 rounded-lg mx-auto my-5 overflow-hidden">
        <div className="flex justify-center">
            <div className="w-5/6 p-10 ">
                <div className="mx-auto flex flex-wrap justify-between my-3 mt-2">
                    <h1 className="text-xl font-bold ">Job Details:</h1>
                    <Link className="w-14 border bg-white hover:bg-gray-300 rounded-lg border-gray-500 text-center" to={'/vacancy/list'}>Back</Link>
                </div>
        <div className="mx-auto flex flex-wrap gap-2 items-center justify-start mb-5">
        {data !== null &&
        <>
            <div>    
                <div>
                    <img className="w-36" src={data.company_image_url}></img>
                    <h1 className="text-lg font-bold">
                        {data.company_name}
                    </h1>
                </div>
                <div className="my-3 grid grid-cols-1 gap-2 flex-block gap-2">
                    <div>
                        <label className="font-semibold">Job Title:</label>
                        <h1 className="text-md">{data.title}</h1>
                    </div>
                    <div>
                        <label className="font-semibold">Job Description:</label>
                        <p className="text-justify normal-case">{data.job_description}</p>
                    </div>
                    <div>
                        <label className="font-semibold">Job Qualification:</label>
                        <p className="text-justify normal-case">{data.job_qualification}</p>
                    </div>
                    <div>
                        <label className="font-semibold">Job Tenure:</label>
                        <p className=" normal-case">{data.job_tenure}</p>
                    </div>
                    <div>
                        <label className="font-semibold">Job Location:</label>
                        <p className="capitalize">{data.company_city}</p>
                    </div>
                    <div>
                        <label className="font-semibold">Job Type:</label>
                        <p className=" normal-case">{data.job_type}</p>
                    </div>
                    <div>
                        <label className="font-semibold">Salary Range:</label>
                        <p>{handlePrice(data.salary_min)} - {handlePrice(data.salary_max)}</p>
                    </div>
                    <div className="w-24">
                        <label className="font-semibold">Job Status:</label>
                        {handleJobStatus(data.job_status)}
                    </div>
                </div>
            </div>
        </>
        }
        </div>
        </div>
        </div>
        </section>
        </>
    )
}

export default VacancyDetails