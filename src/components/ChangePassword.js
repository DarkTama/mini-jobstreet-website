import React,{useContext} from "react";
import { GlobalContext } from "../context/GlobalContext";

const ChangePassword = () =>{
    const{state, handleFunction} = useContext(GlobalContext)
    const{input,success,validasi} = state
    const{handleInput,handleChangePassword} = handleFunction
    return(
        <>
            <div className="bg-white overflow-hidden rounded-lg p-5 my-5">
                <form onSubmit={handleChangePassword}>
                    <label>Change Password</label>
                    <hr/>
                    <p className="text-red-500 font-bold">{validasi}</p>
                    <p className="text-green-500 font-bold">{success}</p>
                    <br/>
                    <label>Current Password</label><br/>
                    <input minLength={8} type="password" value={input.current_password} required onChange={handleInput} name="current_password" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500"/><br/><br/>
                    <label>New Password</label><br/>
                    <input minLength={8} type="password" value={input.new_password} required onChange={handleInput} name="new_password" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500"/><br/><br/>
                    <label>Confirm New Password</label><br/>
                    <input minLength={8} type="password" value={input.new_confirm_password} required onChange={handleInput} name="new_confirm_password" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" />
                    <button className="my-5 w-20 h-12 bg-purple-500 text-white border border-gray-300 rounded-lg hover:bg-purple-600" type="submit">Submit</button>
                </form>
                
            </div>
        </>
    )
}

export default ChangePassword