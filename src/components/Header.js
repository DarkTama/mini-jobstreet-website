import React from "react";
import {Link} from 'react-router-dom';
import Cookies from "js-cookie";

const Header = () =>{
    return(
        <>
            <nav className="font-sans flex flex-col text-center sm:flex-row sm:text-left sm:justify-between py-4 px-6 bg-white shadow sm:items-baseline w-full bg-blue-500">
            <div className="mb-2 sm:mb-0">
                {/* <Link to="/" className="text-2xl no-underline text-grey-darkest hover:text-blue-dark">Home</Link> */}
            </div>
            <div>
                {!Cookies.get('token') && <Link className="text-lg no-underline text-grey-darkest focus:text-white hover:text-white ml-2" to={'/login'}>Login</Link>}
                {Cookies.get('token') && <Link to="/dashboard" className="text-lg no-underline focus:text-white text-grey-darkest hover:text-white hover:text-blue-dark ml-2">Dashboard</Link>}
                <Link to="/" className="text-lg no-underline text-grey-darkest hover:text-white focus:text-white ml-2">Home</Link>
            </div>
            </nav>
        </>
    )
}
export default Header