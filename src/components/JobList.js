import React,{useContext} from "react";
import { GlobalContext } from "../context/GlobalContext";
import axios from "axios";

const JobList = () =>{
    const{state, handleFunction} = useContext(GlobalContext)
    const{data, filters, filterCompany, filterType, filterTenure, fetch} = state
    const{handleTruncate, filterBySearch, handleJobStatus, handleVacancyDetails, handleChangeFilter, handleFilter} = handleFunction
    return(
        <> 
        <section className="bg-gray-200 rounded-lg mx-auto overflow-hidden">
        <div className="flex justify-center">
            <div className="w-5/6 p-10 ">
                <div className="mx-auto my-3 mt-2">
                    <input required onChange={filterBySearch} placeholder="Type job name..." name="searchbytitle" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500"/><br/>
                    <form onSubmit={handleFilter} className="grid grid-cols-3">
                        <select onChange={handleChangeFilter} value={filters.job_location} name="job_location" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500">
                            <option Value="">Select Job Location. . .</option>
                            {filterCompany !== null &&(
                                <>
                                {filterCompany.map((res)=>{
                                    return(
                                        <>
                                            <option defaultValue={`${res}`}>{res}</option>
                                        </>
                                    )
                                })}
                                </>
                            )}
                        </select>
                        <select onChange={handleChangeFilter} value={filters.job_tenure} name="job_tenure" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500">
                            <option Value="">Select Job Tenure. . .</option>
                            {filterTenure !== null &&(
                                <>
                                {filterTenure.map((res)=>{
                                    return(
                                        <>
                                            <option defaultValue={`${res}`}>{res}</option>
                                        </>
                                    )
                                })}
                                </>
                            )}
                        </select>
                        <select onChange={handleChangeFilter} value={filters.job_type} name="job_type" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500">
                            <option Value="">Select Job Type. . .</option>
                            {filterType !== null &&(
                                <>
                                {filterType.map((res)=>{
                                    return(
                                        <>
                                            <option defaultValue={`${res}`}>{res}</option>
                                        </>
                                    )
                                })}
                                </>
                            )}
                        </select>
                        <button className="w-14 h-7 col-span-3 border border-gray-500 hover:bg-blue-500 bg-blue-400 rounded-lg" type="submit">Filter</button>
                    </form>
                    <button className="w-14 h-7 col-span-3 border border-gray-500 hover:bg-red-500 bg-red-400 rounded-lg" onClick={fetch}>Reset</button>
                    <br/>
                    <h1 className="text-xl font-bold ">Job Lists:</h1>
                </div>
                <div className="mx-auto flex flex-wrap gap-2 items-center justify-start mb-5">
                {data !== null && data.map((res)=>{
                    return(
                        <>
                            <div className="overflow-hidden shadow-lg rounded-lg h-80 w-48 opacity-100 m-auto ">
                                <div className="bg-white dark:bg-gray-800 grid grid-cols-1 grid-rows-4 justify-between w-full h-full p-4">
                                    <div>
                                        <p className="text-indigo-500 text-md text-left normal-case font-medium">
                                            {handleTruncate(res.title,20)}
                                        </p>
                                        <p className="text-gray-800 dark:text-white text-left text-md font-medium mb-2">
                                            {handleTruncate(res.company_name,20)}
                                        </p>
                                    </div>
                                    <div>
                                        <p className="text-gray-600 dark:text-gray-300 text-justify font-light text-md">
                                            {handleTruncate(res.job_description, 50)}
                                        </p>
                                    </div>
                                    <div className="flex flex-wrap justify-starts items-center mt-4">
                                        {handleJobStatus(res.job_status)}
                                    </div>
                                    <div className="py-3 flex flex-wrap justify-end">
                                        <button className="w-20 h-7 border border-gray-500 hover:bg-gray-300 bg-gray-200 rounded-lg" value={res.id} onClick={handleVacancyDetails}>Details</button>
                                        {/* <p className="text-gray-600 dark:text-gray-300 font-light text-sm">
                                            {handlePrice(res.salary_min)} - {handlePrice(res.salary_max)}
                                        </p> */}
                                    </div>
                                </div>
                            </div>
                        </>
                    )
                })}
                </div>
            </div>
        </div>
        </section>
        </>
    )
}
export default JobList