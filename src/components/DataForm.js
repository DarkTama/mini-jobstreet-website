import axios from "axios";
import React,{useContext, useEffect} from "react";
import { useParams } from "react-router-dom";
import { GlobalContext } from "../context/GlobalContext";

const DataForm = () =>{
    let {idEdit} = useParams()
    const{state, handleFunction} = useContext(GlobalContext)
    const{data, setCurrentId, setFetchStatus, setInput, input, success, validasi} = state
    const{handleInput,handleSubmit} = handleFunction
    useEffect(()=>{
        if(idEdit !== undefined){
            axios.get(`https://dev-example.sanbercloud.com/api/job-vacancy/${idEdit}`)
            .then((res)=>{
                let data = res.data
                setInput({
                    company_city:data.company_city,
                    company_name:data.company_name,
                    company_image_url:data.company_image_url,
                    job_description:data.job_description,
                    job_qualification:data.job_qualification,
                    job_status:data.job_status,
                    job_tenure:data.job_tenure,
                    job_type:data.job_type,
                    salary_max:data.salary_max,
                    salary_min:data.salary_min,
                    title:data.title,
                })
            })
            .catch((err)=>{
                console.log(err)
            })
        }
        else{
            // setCurrentId(-1)
            setFetchStatus(true)
        }
    },[])
    return(
        <>
            <form onSubmit={handleSubmit} className="">
                <p className="font-bold text-green-500">{success}</p>
                <p className="font-bold text-red-500">{validasi}</p>
                <div className="bg-white overflow-hidden rounded-lg p-5 my-5">
                    <label>Form Company</label>
                    <hr/><br/>
                    <label>Nama Company</label><br/>
                    <input required value={input.company_name} onChange={handleInput} name="company_name" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500"/><br/><br/>
                    <label>Lokasi Company</label><br/>
                    <input required value={input.company_city} onChange={handleInput} name="company_city" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500"/><br/><br/>
                    <label>Image Company</label><br/>
                    <input required value={input.company_image_url} onChange={handleInput} name="company_image_url" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" />
                </div>
                <div className="bg-white overflow-hidden rounded-lg p-5 my-5">
                    <label>Form Job</label>
                    <hr/><br/>
                    <label>Title</label><br/>
                    <input required value={input.title} onChange={handleInput} name="title" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" /><br/><br/>
                    <label>Job Description</label><br/>
                    <textarea required value={input.job_description} onChange={handleInput} name="job_description" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500"></textarea><br/>
                    <label>Job Qualification</label><br/>
                    <input required value={input.job_qualification} onChange={handleInput} name="job_qualification" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" /><br/><br/>
                    <label>Job Tenure</label><br/>
                    <input required value={input.job_tenure} onChange={handleInput} name="job_tenure" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" /> <br/><br/>
                    <label>Job Type</label><br/>
                    <input required value={input.job_type} onChange={handleInput} name="job_type" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" /> <br/><br/>
                    <label>Salary</label><br/>
                    <input required value={input.salary_min} onChange={handleInput} type="number" placeholder="Minimum" name="salary_min" className="w-1/2 bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" />
                    <input required value={input.salary_max} onChange={handleInput} type="number" placeholder="Maximum" name="salary_max" className="w-1/2 bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" /><br/><br/>
                    <label>Job Status</label>
                    <input required value={input.job_status} onChange={handleInput} type="number" min={0} max={1} name="job_status" className="w-full bg-gray-100 border border-gray-300 rounded focus:outline-none focus:bg-white focus:border-purple-500" />
                </div>
                <button className="w-20 h-12 bg-purple-500 text-white border border-gray-300 rounded-lg hover:bg-purple-600" type="submit">Submit</button>
            </form>
        </>
    )
}

export default DataForm