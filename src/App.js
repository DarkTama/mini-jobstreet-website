import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom';
import LandingPage from './pages/LandingPage';
import { GlobalProvider } from './context/GlobalContext';
import Header from './components/Header';
import DashboardPage from './pages/DashboardPage';
import DataTable from './components/DataTable';
import DataForm from './components/DataForm';
import Cookies from 'js-cookie';
import Login from './components/Login';
import Register from './components/Register';
import ChangePassword from './components/ChangePassword';
import Logout from './components/Logout';
import Layout from './components/Layout';
import VacancyListPage from './pages/VacancyListPage';
import VacancyDetails from './components/VacancyDetails';
import JobList from './components/JobList';

function App() {
  const LoginRoute = (props) =>{
    if(Cookies.get('token') !==undefined){
      return <Navigate to={'/'} />
    } else if (Cookies.get('token')=== undefined){
      return props.children
    }
  }
  const RequireLogin = (props)=>{
    if(Cookies.get('token') === undefined){
      return <Navigate to={'/login'} />
    } else if (Cookies.get('token') !== undefined){
      return props.children
    }
  }
  return (
    <>
      <BrowserRouter>
        <GlobalProvider>
          {/* <Header /> */}
            <Routes>
              <Route index element={
                <Layout>
                  <LandingPage />
                </Layout>
              } />
              <Route path='vacancy' element={
                <Layout>
                  <VacancyListPage />
                </Layout>
              }>
                  <Route path='list' element={<JobList />} />
                  <Route path=':idVacancy' element={<VacancyDetails />} />
              </Route>
              <Route path='dashboard' element={
                <RequireLogin>
                  <Layout>
                    <DashboardPage />
                  </Layout>
                </RequireLogin>
                }>
                <Route path="list-job-vacancy" element={<DataTable />} />
                <Route path="list-job-vacancy/form" element={<DataForm />}>
                  <Route path=':idEdit' element={<DataForm />} />
                </Route>
                {/* <Route path="profile" /> */}
                <Route path="change-password" element={<ChangePassword />} />
              </Route>
              <Route path='login' element={
                <LoginRoute>
                  <Login />
                </LoginRoute>
              }/>
              <Route path='register' element={<Register />} />
              <Route path='logout' element={<Logout />} />
            </Routes>
        </GlobalProvider>
      </BrowserRouter>
    </>
  );
}

export default App;
