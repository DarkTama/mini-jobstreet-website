# Mini Jobstreet Website

a final project assignment during ReactJS Bootcamp period at SanberCode. The project was built using Reactjs. 


# Deployed site: 

- https://final-project-ekatama.netlify.app 


# What was implemented:

- state management using globalcontext 
- nested route 
- restapi usage using axios
- authentication (login, registration) with token using cookie-js
